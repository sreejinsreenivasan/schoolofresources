# Generated by Django 3.1.2 on 2020-11-02 10:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_auto_20201102_1208'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='course_image',
            field=models.ImageField(null=True, upload_to='media/uploads/raw'),
        ),
        migrations.AddField(
            model_name='course',
            name='course_thumbnail',
            field=models.ImageField(null=True, upload_to='media/uploads/thumbnail'),
        ),
    ]
