# Generated by Django 3.1.2 on 2021-03-25 07:39

from django.db import migrations, models
import project.storage_backends


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0030_auto_20210324_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='course_image',
            field=models.ImageField(null=True, storage=project.storage_backends.PrivateMediaStorage(), upload_to='uploads/raw/%d%m/%y'),
        ),
        migrations.AlterField(
            model_name='course',
            name='course_thumbnail',
            field=models.ImageField(null=True, storage=project.storage_backends.PrivateMediaStorage(), upload_to='uploads/thumbnail/%d/%m/%y'),
        ),
    ]
