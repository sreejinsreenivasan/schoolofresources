# Generated by Django 3.1.2 on 2020-11-04 07:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0005_auto_20201103_1413'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='course_image',
            field=models.ImageField(null=True, upload_to='uploads/raw/%d%m/%y'),
        ),
        migrations.AlterField(
            model_name='course',
            name='course_thumbnail',
            field=models.ImageField(null=True, upload_to='uploads/thumbnail/%d/%m/%y'),
        ),
        migrations.AlterField(
            model_name='module',
            name='course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='modules', to='courses.course'),
        ),
    ]
