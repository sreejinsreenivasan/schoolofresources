# Generated by Django 3.1.2 on 2020-11-11 08:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0013_auto_20201109_1225'),
    ]

    operations = [
        migrations.RenameField(
            model_name='section',
            old_name='type',
            new_name='content_type',
        ),
    ]
