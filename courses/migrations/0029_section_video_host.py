# Generated by Django 3.1.2 on 2021-03-18 08:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0028_auto_20210119_1117'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='video_host',
            field=models.CharField(choices=[('YOUTUBE', 'Youtube'), ('VIMEO', 'Vimeo')], default='YOUTUBE', max_length=50),
        ),
    ]
