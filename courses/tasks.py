from celery import shared_task
from app.email import send_payment_email, send_payment_confirmation_mail
# @shared_task(name="course_purchase_successfull")
# def course_purchase_successfull():





@shared_task(name="send_payment_link")
def send_payment_link(recipient_list,code):
    send_payment_email([recipient_list],code)


@shared_task(name="send_payment_success")
def send_payment_success(recipient_list,payment_id):
    send_payment_confirmation_mail([recipient_list],payment_id)
