from django.db import models
from PIL import Image
import io, uuid
from django.core.files.uploadedfile import InMemoryUploadedFile
from hashlib import md5
from project.storage_backends import PrivateMediaStorage
# from django.contrib.postgres.fields import JSONField

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=100)
    created_by = models.ForeignKey("app.User", on_delete=models.DO_NOTHING)


class Course(models.Model):
    STATUS_CHOICE = [
        ("INACTIVE", "inactive"),
        ("PUBLISHED", "published"),
        ("ACTIVE", "active"),
    ]
    trainer = models.ForeignKey(
        "app.Trainer", on_delete=models.CASCADE, null=True, blank=True
    )
    category = models.ForeignKey("courses.Category", on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=250)
    created_by = models.ForeignKey("app.User", on_delete=models.DO_NOTHING)
    price = models.FloatField(default=0.00)
    offer_price = models.FloatField(default=0.00)
    description = models.TextField()
    course_image = models.ImageField(upload_to="uploads/raw/%d%m/%y", null=True,storage=PrivateMediaStorage())
    course_thumbnail = models.ImageField(
        upload_to="uploads/thumbnail/%d/%m/%y", null=True,storage=PrivateMediaStorage()
    )
    status = models.CharField(max_length=50, choices=STATUS_CHOICE, default="ACTIVE")
    uid = models.UUIDField(default=uuid.uuid4, editable=False)

    @property
    def save_thumbnail(self):
        im = Image.open(self.course_image)
        im.thumbnail((200, 200), Image.ANTIALIAS)
        output = io.BytesIO()
        im.save(output, format="jpeg", quality=95)
        output.seek(0)
        self.course_thumbnail = InMemoryUploadedFile(
            output,
            "ImageField",
            "%s.jpg" % self.course_image.name,
            "image/jpeg",
            0,
            None,
        )
        self.save()

    @property
    def get_sections(self):
        sections = []
        for m in self.modules.all():
            for s in m.sections.all():
                sections.append(s)
        return sections

    @property
    def number_of_sections(self):
        return len(self.get_sections)


class ModuleDescription(models.Model):
    course = models.ForeignKey("courses.Course", on_delete=models.CASCADE)
    content = models.TextField()


class Module(models.Model):
    course = models.ForeignKey(
        "courses.Course", related_name="modules", on_delete=models.DO_NOTHING
    )
    created_by = models.ForeignKey("app.User", on_delete=models.DO_NOTHING)
    title = models.TextField()


class Section(models.Model):

    TYPE_CHOICES = (("VIDEO_LINK", "Video Link"), ("PLAIN_TEXT", "Plain Text"))
    VIDEO_HOST_CHOICES = [("YOUTUBE","Youtube"),('VIMEO','Vimeo'),('S3','S3'),('OWN_SERVER','Own Server')]
    module = models.ForeignKey(
        "courses.Module", related_name="sections", on_delete=models.CASCADE
    )
    title = models.TextField()
    is_completed = models.BooleanField(default=False)
    content_type = models.CharField(
        max_length=50, choices=TYPE_CHOICES, default="VIDEO_LINK"
    )
    content = models.JSONField(default=list)
    video_link = models.CharField(max_length=300, null=True)
    video_host = models.CharField(max_length=50,choices=VIDEO_HOST_CHOICES,default="YOUTUBE")


class Enrollment(models.Model):
    user = models.ForeignKey(
        "app.User", related_name="enrolled_courses", on_delete=models.CASCADE
    )
    course = models.ForeignKey(
        "courses.Course", related_name="enrollments", on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(auto_now_add=False, auto_now=True)
    is_completed = models.BooleanField(default=False)
    sections = models.JSONField(default=list)
    last_viewed_section = models.IntegerField(default=0)
    last_viewed = models.DateTimeField(auto_now=True)
    uid = models.UUIDField(default=uuid.uuid4, editable=False)

    @property
    def get_progress(self):
        total_sections = self.course.number_of_sections
        completed_sections = len(
            list(filter(lambda x: x["is_completed"] == True, self.sections))
        )
        percetage = (completed_sections / total_sections) * 100
        return percetage

    @property
    def get_modules_with_sections(self):
        matching_sections = list(self.sections)
        course = self.course
        module_data = []
        for module in course.modules.all():
            data = {"id": module.id, "title": module.title}
            sections = []
            for section in module.sections.all():
                try:
                    match = list(
                        filter(lambda x: (x["id"] == section.id), matching_sections)
                    )[0]
                    sections.append(
                        {
                            "id": section.id,
                            "title": section.title,
                            "is_completed": match["is_completed"],
                            "content_type": section.content_type,
                        }
                    )
                except IndexError:
                    self.sections.append({"id": section.id, "is_completed": False})
                    sections.append(
                        {
                            "id": section.id,
                            "title": section.title,
                            "is_completed": False,
                            "content_type": section.content_type,
                        }
                    )

            module_data.append({**data, "sections": sections})
            return module_data


class Purchase(models.Model):
    PAYMENT_GATEWAY_CHOICES = [("RAZORPAY", "Razorpay")]
    enrollment = models.ForeignKey(
        "courses.Enrollment", on_delete=models.DO_NOTHING, related_name="purchase"
    )
    payment_gateway = models.CharField(
        max_length=24, choices=PAYMENT_GATEWAY_CHOICES, default="RAZORPAY"
    )
    payment_id = models.CharField(max_length=100)
    order_id = models.CharField(max_length=100)
    signature = models.TextField()


class ProductLinks(models.Model):
    product = models.ForeignKey(
        "courses.Course", related_name="shared_urls", on_delete=models.CASCADE
    )
    full_url = models.URLField(unique=True)
    url_hash = models.URLField(unique=True)
    clicks = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        "app.User", related_name="shared_links", on_delete=models.DO_NOTHING
    )

    @property
    def clicked(self):
        self.clicks += 1
        self.save()

    def save(self, *args, **kwargs):
        if not self.id:
            self.url_hash = md5(self.full_url.encode()).hexdigest()[:10]

        return super().save(*args, **kwargs)

class S3VideoUpload(models.Model):
    upload_at = models.DateTimeField(auto_now_add=True)
    file = models.FileField(storage=PrivateMediaStorage())
    name = models.TextField()
    is_active = models.BooleanField(default=True)

    