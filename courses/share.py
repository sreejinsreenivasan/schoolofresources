from django.views import View
from django.shortcuts import render, redirect, HttpResponse, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Course, ProductLinks
from django.http import JsonResponse


class ShareProduct(LoginRequiredMixin, View):
    login_url = "app:login"
    template = "share-product.html"
    base_url = ""
    host = ""

    def get(self, request, uid):
        self.host = request.headers["Host"]
        self.base_url = f"https://{self.host}"
        product = get_object_or_404(Course, uid=uid)
        try:
            link = ProductLinks.objects.get(product=product, created_by=request.user)
        except ProductLinks.DoesNotExist:

            full_url = f"{self.base_url}/courses/shared-course/{request.user.refferal_code}/{product.uid}"
            link = ProductLinks.objects.create(
                product=product, full_url=full_url, created_by=request.user
            )
        context = {
            "link": f"{self.base_url}/courses/course/{link.url_hash}",
            "title": product.name,
        }
        # return render(request, self.template, context)
        return JsonResponse(status=200, data=context, safe=False)


class ProdcutDetailView(View):
    template = "course-detail.html"
    base_url = ""
    host = ""

    def get(self, request, id):
        self.host = request.headers["Host"]
        self.base_url = f"https://{self.host}"
        obj = get_object_or_404(ProductLinks, url_hash=id)
        request.session["shared_product_id"] = id
        obj.clicked
        has_share_link = True
        course = obj.product
        context = {
            "course": course,
            "is_enrolled": False,
            "has_share_link": has_share_link,
            "share_link": obj.url_hash,
            "webmaster": {
                "url": f"{self.base_url}/courses/course/{obj.url_hash}",
                "title": course.name,
                "description": course.description,
                "image_url": f"{self.base_url}/{course.course_image.url}",
            },
        }
        return render(request, self.template, context)