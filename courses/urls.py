from django.urls import path
from .views import (
    CourseView,
    CourseDetailView,
    EnrolledCourseDetail,
    GetEnrolled,
    SectionStatusUpdate,
    LoadSection,
    LoadNextSection,
    LoadPreviousSection,
)
from .payment import BuyCourse, PaymentSuccess, BuyShareDCourse
from .share import ShareProduct, ProdcutDetailView

app_name = "courses"

urlpatterns = [
    path("courses/", CourseView.as_view(), name="coursesall"),
    path("course-detail/<str:id>/", CourseDetailView.as_view(), name="detail"),
    path("continue-course/<str:id>/", EnrolledCourseDetail.as_view(), name="continue"),
    path("enroll-course/", GetEnrolled.as_view(), name="enrollcourse"),
    path("update-status/", SectionStatusUpdate.as_view(), name="updatestatus"),
    path("load-section/", LoadSection.as_view(), name="loadsection"),
    path("load-next-section/", LoadNextSection.as_view(), name="nextsection"),
    path("load-previous-section/", LoadPreviousSection.as_view(), name="loadprevious"),
    path("checkout/<str:id>", BuyCourse.as_view(), name="checkout"),
    path(
        "checkout/<str:id>/<str:share_link>", BuyShareDCourse.as_view(), name="checkout"
    ),
    path("payment-success/", PaymentSuccess.as_view(), name="success"),
    path("share/<str:uid>", ShareProduct.as_view(), name="share"),
    path("course/<str:id>", ProdcutDetailView.as_view(), name="product"),
]
