from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django.shortcuts import render, get_object_or_404, redirect
from .models import Course, Enrollment, Purchase, ProductLinks
import razorpay
from decouple import config
from django.http import JsonResponse


class BuyCourse(LoginRequiredMixin, View):
    login_url = "app:login"
    template = "buycourse.html"

    def get(self, request, id):
        try:
            en = Enrollment.objects.get(course__uid=id, user=request.user)
            if en and en is not None:
                return redirect("courses:continue", en.uid)
        except Enrollment.DoesNotExist:
            course = get_object_or_404(Course, uid=id)
            context = {"course": course}
            return render(request, self.template, context)

    def post(self, request, id):
        course = get_object_or_404(Course, uid=id)
        currency = request.POST["currency"]
        amount = course.price * 100
        DEBUG = config('DEBUG')
        if DEBUG:
            RZP_KEY = config("RAZORPAY_TEST_KEY")
            RZP_SECRET = config("RAZORPAY_TEST_SECRET_KEY")
        else:
            RZP_KEY = config("RAZORPAY_KEY")
            RZP_SECRET = config("RAZORPAY_SECRET_KEY")
        client = razorpay.Client(auth=(RZP_KEY, RZP_SECRET))
        payment = client.order.create(
            {"amount": round(amount), "currency": currency, "payment_capture": 1}
        )
        data = {"course_id": course.id, "payment": payment, "rzp_key": RZP_KEY}
        return JsonResponse(data=data, safe=False)


class BuyShareDCourse(LoginRequiredMixin, View):
    login_url = "app:login"
    template = "buycourse.html"

    def get(self, request, id, share_link):
        try:
            en = Enrollment.objects.get(course__uid=id, user=request.user)
            if en and en is not None:
                return redirect("courses:continue", en.uid)
        except Enrollment.DoesNotExist:
            product = get_object_or_404(ProductLinks, url_hash=share_link)
            context = {"course": product.product, "share_link": product.url_hash}
            return render(request, self.template, context)


class PaymentSuccess(LoginRequiredMixin, View):
    login_url = "app:login"

    def post(self, request):
        shared_link = request.POST.get("share_link", None)
        user = request.user
        course = get_object_or_404(Course, uid=request.POST["course_id"])
        sections = []
        for m in course.modules.all():
            for s in m.sections.all():
                sections.append({"id": s.id, "is_completed": False})
        try:
            last_viewed_section = sections[0]["id"]
        except:
            last_viewed_section = 0
        enrollment = Enrollment.objects.create(
            user=user,
            course=course,
            sections=sections,
            last_viewed_section=last_viewed_section,
        )
        Purchase.objects.create(
            enrollment=enrollment,
            payment_id=request.POST["payment_id"],
            order_id=request.POST["order_id"],
            signature=request.POST["signature"],
        )
        try:
            if shared_link is not None:
                product = ProductLinks.objects.get(url_hash=shared_link)
                shared_person = product.created_by
                if user != shared_person:
                    shared_person.primary_wallet += (course.offer_price * 20) / 100
                    shared_person.save()
        except ProductLinks.DoesNotExist:
            pass
        try:
            refer_person = course.trainer.user.refferal_person
            refer_person.primary_wallet += (course.offer_price * 5) / 100
            refer_person.save()

        except AttributeError:
            pass
        redirect_url = f"/courses/continue-course/{enrollment.uid}/"
        return JsonResponse(data={"redirect_url": redirect_url}, safe=False)
