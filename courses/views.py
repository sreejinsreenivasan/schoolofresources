from django.shortcuts import render, HttpResponse, get_object_or_404
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Course, Section, Enrollment
from django.http import JsonResponse, Http404
from django.core import serializers
import json
from django.core.exceptions import ValidationError


class CourseView(View):
    template_name = "courses.html"

    def get(self, request):
        courses = Course.objects.filter(status="ACTIVE")
        context = {"courses": courses}
        return render(request, self.template_name, context)


class CourseDetailView(View):
    template_name = "course-detail.html"
    base_url = ""
    host = ""

    def get(self, request, id):
        self.host = request.headers["Host"]
        self.base_url = f"https://{self.host}"
        try:
            course = get_object_or_404(Course, uid=id)
            if request.user.is_authenticated:
                en = Enrollment.objects.get(user=request.user, course=course)
                is_enrolled = True
            else:
                en = {}
                is_enrolled = False
        except Enrollment.DoesNotExist:
            is_enrolled = False
            en = {}

        except ValidationError:
            raise Http404()

        context = {
            "course": course,
            "is_enrolled": is_enrolled,
            "en": en,
            "page_title": course.name,
            "webmaster": {
                "url": f"{self.base_url}/courses/course-detail/{course.uid}/",
                "title": course.name,
                "description": course.description,
                "image_url": f"{self.base_url}{course.course_image.url}",
            },
        }
        return render(request, self.template_name, context)


class GetEnrolled(LoginRequiredMixin, View):
    login_url = "app:login"

    def post(self, request):
        user = request.user
        course = Course.objects.prefetch_related("modules").get(
            id=request.POST.get("course_id")
        )
        try:
            obj = Enrollment.objects.get(user=user, course=course)
        except Enrollment.DoesNotExist:
            data = []
            for module in course.modules.all():
                for section in module.sections.all():
                    data.append({"id": section.id, "is_completed": False})
            obj = Enrollment.objects.create(user=user, course=course, sections=data)
        res = {"enrollment_id": obj.id}
        return JsonResponse(status=201, data=res, safe=False)


class EnrolledCourseDetail(LoginRequiredMixin, View):
    login_url = "app:login"
    template_name = "enrolled-course-detail.html"

    def get(self, request, id):
        try:
            obj = get_object_or_404(Enrollment, uid=id, user=request.user)
            course = obj.course
            matching_sections = list(obj.sections)
            module_data = []
            for module in course.modules.all():
                data = {"id": module.id, "title": module.title}
                sections = []
                for section in module.sections.all():
                    try:
                        match = list(
                            filter(lambda x: (x["id"] == section.id), matching_sections)
                        )[0]
                        sections.append(
                            {
                                "id": section.id,
                                "title": section.title,
                                "is_completed": match["is_completed"],
                                "content_type": section.content_type,
                            }
                        )
                    except IndexError:
                        obj.sections.append({"id": section.id, "is_completed": False})
                        sections.append(
                            {
                                "id": section.id,
                                "title": section.title,
                                "is_completed": False,
                                "content_type": section.content_type,
                            }
                        )

                module_data.append({**data, "sections": sections})
            try:
                _section = Section.objects.get(id=obj.last_viewed_section)
                last_viewed_section = {
                    "id": _section.id,
                    "title": _section.title,
                    "content_type": _section.content_type,
                    "content": _section.content,
                }
            except Section.DoesNotExist:
                try:

                    last_viewed_section = module_data[0]["sections"][0]
                    obj.last_viewed_section = last_viewed_section["id"]
                except:
                    last_viewed_section = 0
                    obj.last_viewed_section = 0
            obj.save()
            context = {
                "course": obj.course,
                "enrolled_course": module_data,
                "enrollment_id": obj.id,
                "last_viewed_section": json.dumps(last_viewed_section),
                "total_number_of_sections": len(module_data),
                # "progress": obj.get_progress,
            }
            return render(request, self.template_name, context)
        except Enrollment.DoesNotExist:
            raise Http404()


class SectionStatusUpdate(LoginRequiredMixin, View):
    login_url = "app:login"

    def post(self, request):
        print(request.POST)
        section_id = request.POST["section_id"]
        obj = get_object_or_404(Enrollment, id=request.POST["enrollment_id"])
        section = list(filter(lambda x: x["id"] == int(section_id), obj.sections))
        try:
            s = section[0]
            index = obj.sections.index(s)
            s["is_completed"] = not s["is_completed"]
            obj.sections[index] = s
            obj.save()
        except IndexError:
            JsonResponse(status=400, data={})
        return JsonResponse(data={"is_completed": True})


class LoadSection(LoginRequiredMixin, View):
    login_url = "app:login"

    def post(self, request):
        enrollment = get_object_or_404(Enrollment, id=request.POST["enrollment_id"])
        section = get_object_or_404(Section, id=request.POST["section_id"])
        enrollment.last_viewed_section = section.id
        current_section = list(
            filter(
                lambda x: x["id"] == enrollment.last_viewed_section, enrollment.sections
            )
        )[0]
        index = enrollment.sections.index(current_section)
        current_section["is_completed"] = True
        enrollment.sections[index] = current_section
        enrollment.save()
        data = {
            "id": section.id,
            "title": section.title,
            "content_type": section.content_type,
            "content": section.content,
            "video_link":section.video_link
        }
        return JsonResponse(data=data, safe=False)


class LoadNextSection(LoginRequiredMixin, View):
    login_url = "app:login"

    def post(self, request):
        en = get_object_or_404(Enrollment, id=request.POST["enrollment_id"])
        current_section = Section.objects.get(id=en.last_viewed_section)
        course = en.course
        sections_list = []
        for m in course.modules.all():
            for s in m.sections.all():
                sections_list.append(s)
        index = sections_list.index(current_section)
        length_of_sections = len(sections_list)
        if (index + 1) < length_of_sections:
            next_section = sections_list[index + 1]
        else:
            next_section = sections_list[length_of_sections - 1]
        try:
            match = list(filter(lambda x: x["id"] == next_section.id, en.sections))[0]
        except IndexError:
            en.sections.append({"id": next_section.id, "is_completed": True})
        data = {
            "id": next_section.id,
            "title": next_section.title,
            "is_completed": True,
            "content": next_section.content,
            "content_type": next_section.content_type,
        }
        en.last_viewed_section = next_section.id
        en.save()
        return JsonResponse(data=data, safe=False)


class LoadPreviousSection(LoginRequiredMixin, View):
    login_url = "app:login"

    def post(self, request):
        en = get_object_or_404(Enrollment, id=request.POST["enrollment_id"])
        current_section = Section.objects.get(id=en.last_viewed_section)
        course = en.course
        sections_list = [] 
        for m in course.modules.all():
            for s in m.sections.all():
                sections_list.append(s)
        index = sections_list.index(current_section)
        if index != 0:
            previous_section = sections_list[index - 1]
        else:
            previous_section = sections_list[0]
        try:
            match = list(filter(lambda x: x["id"] == previous_section.id, en.sections))[
                0
            ]
        except IndexError:
            en.sections.appned({"id": previous_section.id, "is_completed": True})
        data = {
            "id": previous_section.id,
            "title": previous_section.title,
            "is_completed": True,
            "content": previous_section.content,
            "content_type": previous_section.content_type,
        }
        en.last_viewed_section = previous_section.id
        en.save()
        return JsonResponse(data=data, safe=False)
