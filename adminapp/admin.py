from django.contrib.auth.models import Permission
from django.contrib import admin
from .models import (
    SignUpBonus,
    SignUpRefferalCommission,
    DirectShareSaleCommission,
    ParentSaleCommision, PaymentLink
)

admin.site.register(Permission)
admin.site.register(SignUpBonus)
admin.site.register(SignUpRefferalCommission)
admin.site.register(DirectShareSaleCommission)
admin.site.register(ParentSaleCommision)
admin.site.register(PaymentLink)