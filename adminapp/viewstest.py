from django.views import View
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.mixins import LoginRequiredMixin


from courses.models import Course,Section, Module


class AddSectionView(LoginRequiredMixin,View):
    template = "adminapp/add-section.html"
    login_url = "adminapp:login"
    redirect_field_name = "next"

    def get(self, request, pk):
        module = get_object_or_404(Module,id=pk)
        context = {"types": Section.TYPE_CHOICES,'hosts':Section.VIDEO_HOST_CHOICES,'module':module}
        return render(request, self.template, context)

    def post(self, request,pk):
        module = get_object_or_404(Module, id=pk)
        section_type = request.POST["section-type"]
        video_link = request.POST['video-link'] if section_type == "VIDEO_LINK" else None
        content = request.POST.get("section-content",[])
        section = Section.objects.create(
            title=request.POST["section-name"],
            content_type=section_type,
            content=content,
            module=module,
            video_link=video_link
        )
        return redirect('adminapp:addmodule', module.course.id)

class SectionDetailView(LoginRequiredMixin, View):
    template = "adminapp/test-edit-section.html"
    login_url = "adminapp:login"
    redirect_field_name = "next"
    
    def get(self, request, pk):
        section = get_object_or_404(Section, id=pk)
        context = {'section':section,"types": Section.TYPE_CHOICES,'hosts':Section.VIDEO_HOST_CHOICES}
        return render(request, self.template, context)

    def post(self, request, pk):
        section = get_object_or_404(Section, id=pk)
        section.content_type = request.POST.get("section-type",section.content_type)
        section.video_link = request.POST.get('video-link',section.video_link)
        section.content = request.POST.get("section-content",[])
        section.title = request.POST.get("section-name",section.title)
        section.save()
        return redirect('adminapp:addmodule',section.module.course.id)
