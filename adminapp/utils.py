from django.shortcuts import render, redirect
from typing import Dict
from django.contrib import messages
from PIL import Image


class Login:
    login_url_name = None
    success_redirect_url_name = ""
    email = ""
    password = ""

    def validate(self, request):
        self.email = request.POST["email"]
        self.password = request.POST["password"]
        cred = {"email": self.email, "password": self.password}
        for key, value in cred.items():
            if len(value) > 0:
                messages.add_message(
                    request, messages.WARNING, f"{key} should not be empty"
                )
                try:
                    return redirect(self.login_url_name)
                except TypeError:
                    raise Exception("login_url_name should not be empty")


def create_thumbnail(image):
    size = (500, 500)
    im = Image.open(image, mode="r")
    im.thumbnail(size)
    return im
