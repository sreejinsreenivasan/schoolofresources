from django.views import View
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from courses.models import Course
from .models import PaymentLink
from project.celery import app as celery_app
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.db.models import Q
from .mixins import StaffUserRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
User = get_user_model()

class CreateClientPayment(LoginRequiredMixin,StaffUserRequiredMixin, View):
    template = "adminapp/create-payments.html"
    login_url = "adminapp:login"
    redirect_field_name = "next"
    sucess_template = "share-payment-link.html"


    def get(self, request):
        courses = Course.objects.filter(status='ACTIVE')
        context = {'courses':courses}
        return render(request, self.template, context)

    def post(self, request):
        data = request.POST
        client_name = data['client']
        client_address = data.get('address', None)
        course = Course.objects.get(id=data['course'])
        # price = data.get('price', None)
        offer_price = data.get('offer-price', None)
        phone = data['phone']
        email = data.get('email', None)
        remarks = data.get('remarks', None)
       
        # user = User.objects.filter(Q(email=email) | Q(username=client_name))
        # if user and user is not None:
        #     messages.add_message(request, messages.INFO, 'User With this email or name already exists!')
        #     return redirect('adminapp:createpayment')

        pay_link = PaymentLink.objects.filter(email=email,course=course)
        if pay_link and pay_link is not None:
            messages.add_message(request, messages.INFO, 'User With this email or name already exists!')
            return redirect('adminapp:createpayment')
        else:
            (payment, created) = PaymentLink.objects.get_or_create(client_name=client_name, client_address=client_address,
            course=course, offer_price=offer_price, phone=phone, email=email, remarks=remarks,created_by=request.user)
            
        return redirect('app:checkout',payment.uuid)