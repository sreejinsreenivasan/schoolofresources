# Generated by Django 3.1.2 on 2020-12-01 10:39

import adminapp.models
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SignUpRefferalCommission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.FloatField(default=0.0)),
                ('created_by', models.OneToOneField(on_delete=models.SET(adminapp.models.get_default_user), to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SignUpBonus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bonus', models.FloatField(default=0.0)),
                ('created_by', models.OneToOneField(on_delete=models.SET(adminapp.models.get_default_user), to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ParentSaleCommision',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('percentage', models.FloatField(default=0.0)),
                ('created_by', models.OneToOneField(on_delete=models.SET(adminapp.models.get_default_user), to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DirectShareSaleCommission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('percentage', models.FloatField(default=0.0)),
                ('created_by', models.OneToOneField(on_delete=models.SET(adminapp.models.get_default_user), to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
