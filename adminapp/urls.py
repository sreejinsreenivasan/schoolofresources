from django.urls import path
from .views import (
    AdminLogin,
    DashBoard,
    CategoryView,
    CourseView,
    AddCourse,
    AddModules,
    AddSection,
    SectionView,
    EditCourse,
    CourseMakeInactive,
    SectionDetailView,
    Category,
    ModuleDetail,
    DeleteModule,
    AddModules2,
    EditSectionView,CategoryDetailView,OrderListView, VideoListView,VideoCreateView, VideoDetailView
)

from .payments import CreateClientPayment
from .viewstest import AddSectionView, SectionDetailView



app_name = "adminapp"


urlpatterns = [
    path("login/", AdminLogin.as_view(), name="login"),
    path("", DashBoard.as_view(), name="dashboard"),
    path("category/", CategoryView.as_view(), name="category"),
    path('category/<int:pk>',CategoryDetailView.as_view()),
    path("courses/", CourseView.as_view(), name="courses"),
    path("add-course/", AddCourse.as_view(), name="addcourse"),
    path("edit-course/<int:pk>", EditCourse.as_view(), name="editcourse"),
    path("make-inactive/<int:pk>", CourseMakeInactive.as_view(), name="makeinactive"),
    path("add-modules/<int:pk>", AddModules.as_view(), name="addmodule"),
    # path("add-section/", AddSection.as_view(), name="addsection"),
    path("section/<int:pk>/", SectionView.as_view(), name="section"),
    path("section-detail/<int:pk>", SectionDetailView.as_view(), name="sectiondetail"),
    path("module/<int:pk>", ModuleDetail.as_view(), name="module"),
    path("module-delete/<int:pk>", DeleteModule.as_view(), name="deletemodule"),
    # path("edit-section/<int:pk>",EditSectionView.as_view(),name="editsection"),


    path("add-modules2/<int:pk>", AddModules2.as_view(), name="addmodule2"),
    path("create-payment", CreateClientPayment.as_view(), name="createpayment"),
    path("orders",OrderListView.as_view(),name="orders"),
    path("videos",VideoListView.as_view(),name="videos"),
    path("add-videos",VideoCreateView.as_view(),name="addvideos"),
    path("video-detail/<int:pk>",VideoDetailView.as_view(),name="videodetail"),




#    test
    path('add-section/<int:pk>',AddSectionView.as_view(),name="addsection"),
    path('edit-section/<int:pk>',SectionDetailView.as_view(),name="editsection"),
]
