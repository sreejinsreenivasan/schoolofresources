from django.db import models
from app.models import User
import uuid
# Create your models here.


def get_default_user():
    return User.objects.filter(is_staff=True)[0]


class SignUpBonus(models.Model):
    bonus = models.FloatField(default=0.00)
    created_by = models.OneToOneField(
        "app.User", on_delete=models.SET(get_default_user)
    )


class SignUpRefferalCommission(models.Model):
    amount = models.FloatField(default=0.00)
    created_by = models.OneToOneField(
        "app.User", on_delete=models.SET(get_default_user)
    )


class DirectShareSaleCommission(models.Model):
    percentage = models.FloatField(default=0.00)
    created_by = models.OneToOneField(
        "app.User", on_delete=models.SET(get_default_user)
    )


class ParentSaleCommision(models.Model):
    percentage = models.FloatField(default=0.00)
    created_by = models.OneToOneField(
        "app.User", on_delete=models.SET(get_default_user)
    )


class PaymentLink(models.Model):
    STATUS = [('INACTIVE', 'Inactive'), ('ACTIVE', 'Active'),
    ('CONVERTED', 'Converted'), ('CLOSED', 'Closed')]
    

    client_name = models.CharField(max_length=50)
    client_address = models.TextField(null=True)
    course = models.ForeignKey("courses.Course", on_delete=models.CASCADE, related_name="course_payments")
    price = models.FloatField(default=0.00)
    offer_price = models.FloatField(default=0.00)
    phone = models.CharField(max_length=12, null=True)
    email = models.EmailField(
        blank=True, max_length=254, verbose_name="email address", null=True
    )
    uuid = models.UUIDField(editable=False,unique=True)
    remarks = models.TextField(null=True)

    status = models.CharField(max_length=20,default="ACTIVE",choices=STATUS)
    created_by = models.ForeignKey("app.User",null=True, related_name="created_payments",on_delete=models.PROTECT)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.uuid = uuid.uuid4()
            super(PaymentLink, self).save(*args, **kwargs)
        else:
            super(PaymentLink, self).save(*args, **kwargs)

