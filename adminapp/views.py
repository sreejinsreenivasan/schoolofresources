from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from django.views import View
from .utils import create_thumbnail
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin
from courses.models import Course, Category, Module, Section, S3VideoUpload
from app.models import Trainer
from PIL import Image
from django.http import JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from .models import *
from django.contrib.auth.mixins import PermissionRequiredMixin
from .permissions import *
from .mixins import StaffUserRequiredMixin
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class AdminLogin(View):
    template_name = "adminapp/login.html"
    next_url = None

    def get_next_url(self, request):
        r_params = request.headers["Referer"].split("=")
        try:
            next_url = r_params[1]
            if next_url:
                self.next_url = str(next_url)
        except IndexError:
            self.next_url = None

    def get(self, request):
        print(request.GET.getlist("next"))
        return render(request, self.template_name)

    def post(self, request):
        self.get_next_url(request)
        email = request.POST["email"]
        password = request.POST["password"]
        remember_me = request.POST.get("remember-me", None)
        cred = {"email": email, "password": password}
        for key, value in cred.items():
            if len(value) < 0:
                messages.add_message(
                    request, messages.WARNING, f"{key} should not be empty"
                )
                return redirect("adminapp:login")
            user = authenticate(email=email, password=password)
            if user and user is not None:
                login(request, user)
                session_expiry = 0
                if remember_me is not None:
                    session_expiry = 1209600
                request.session.set_expiry(session_expiry)
                if user.is_staff:
                    if self.next_url is not None:
                        return redirect(self.next_url)
                    return redirect("adminapp:dashboard")
            messages.add_message(request, messages.ERROR, "Wrong credentials.")
            return redirect("adminapp:login")


class DashBoard(LoginRequiredMixin,StaffUserRequiredMixin, View):
    template_name = "adminapp/dashboard.html"
    login_url = "adminapp:login"
    redirect_field_name = "next"

    def get(self, request):
        return render(request, self.template_name)
    

class CategoryView(LoginRequiredMixin,StaffUserRequiredMixin, View):
    template_name = "adminapp/categories.html"
    login_url = "adminapp:login"
    redirect_field_name = "next"
    # raise_exception = True

    def get(self, request):
        cat = Category.objects.all()
        context = {"categories": cat}
        return render(request, self.template_name, context)

    def post(self, request):
        name = request.POST["category"]
        category = Category.objects.create(name=name, created_by=request.user)
        return redirect("adminapp:category")

class CategoryDetailView(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"

    def delete(self, request, pk):
        obj = get_object_or_404(Category, id=pk)
        obj.delete()
        return JsonResponse(data={},status=200,safe=False)


class CourseView(LoginRequiredMixin, View):
    template_name = "adminapp/courselist.html"
    login_url = "adminapp:login"
    redirect_field_name = "next"

    def get(self, request):
        courses = Course.objects.all()
        context = {"courses": courses}
        return render(request, self.template_name, context)


class AddCourse(LoginRequiredMixin, StaffUserRequiredMixin,View):
    login_url = "adminapp:login"
    redirect_field_name = "next"
    template_name = "adminapp/add-courses.html"

    def get(self, request):
        cats = Category.objects.all()
        trainers = Trainer.objects.all()
        context = {"category": cats, "trainers": trainers}
        return render(request, self.template_name, context)

    def post(self, request):
        name = request.POST["name"]
        price = request.POST["price"]
        offer_price = request.POST["offer-price"]
        trainer = Trainer.objects.get(id=int(request.POST["trainer"]))
        category = Category.objects.get(id=int(request.POST["category"]))
        description = request.POST["description"]
        image = request.FILES["featured-img"]
        course = Course.objects.create(
            name=name,
            price=price,
            offer_price=offer_price,
            trainer=trainer,
            description=description,
            course_image=image,
            created_by=request.user,
            category=category,
        )
        course.save_thumbnail
        return redirect("adminapp:addmodule", course.id)


class EditCourse(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    redirect_field_name = "next"
    template_name = "adminapp/edit-course.html"

    def get(self, request, pk):
        course = get_object_or_404(Course, id=pk)
        context = {"course": course, "status_options": Course.STATUS_CHOICE}
        return render(request, self.template_name, context)

    def post(self, request, pk):
        course = get_object_or_404(Course, id=pk)
        course.name = request.POST.get("name", course.name)
        course.price = request.POST.get("price", course.price)
        course.offer_price = request.POST.get("offer-price", course.offer_price)
        course.category = Category.objects.get(
            id=request.POST.get("category", course.category.id)
        )
        course.status = request.POST.get("status", course.status)
        course.trainer = Trainer.objects.get(
            id=request.POST.get("trainer", course.trainer.id)
        )
        course.status = request.POST.get("status", course.status)

        course.description = request.POST.get("description", course.description)
        if request.FILES:
            course.course_image = request.FILES.get("featured-img")
        course.status = course.status
        course.save()
        course.save_thumbnail
        return redirect("adminapp:editcourse", course.id)


class CourseMakeInactive(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"

    def get(self, request, pk):
        course = get_object_or_404(Course, id=pk)
        course.status = "INACTIVE"
        course.save()
        return redirect("adminapp:courses")


class AddModules(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    redirect_field_name = "next"
    template_name = "adminapp/add-modules.html"

    def get(self, request, pk):
        course = get_object_or_404(Course.objects.prefetch_related(), id=pk)
        context = {"course": course, "types": Section.TYPE_CHOICES,'hosts':Section.VIDEO_HOST_CHOICES}
        return render(request, self.template_name, context)

    def post(self, request, pk):
        course = get_object_or_404(Course, id=request.POST["pk"])
        Module.objects.create(
            course=course, title=request.POST["name"], created_by=request.user
        )
        return HttpResponse(status=201)


class AddModules2(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    redirect_field_name = "next"
    template_name = "adminapp/add-modules2.html"

    def get(self, request, pk):
        course = get_object_or_404(Course.objects.prefetch_related(), id=pk)
        context = {"course": course, "types": Section.TYPE_CHOICES}
        return render(request, self.template_name, context)

    

class AddSection(LoginRequiredMixin, StaffUserRequiredMixin,View):
    login_url = "adminapp:login"
    
    @method_decorator(csrf_exempt)
    def post(self, request):
        print(request.POST)
        module = get_object_or_404(Module, id=request.POST["module_id"])
        section_type = request.POST["section-type"]
        
        video_link = request.POST['video-link'] if section_type == "VIDEO_LINK" else None
        content = request.POST.get("section-content",[])
        section = Section.objects.create(
            title=request.POST["section-name"],
            content_type=section_type,
            content=content,
            module=module,
            video_link=video_link
        )
        data = {"module_id": module.id, "section": section.title}
        return JsonResponse(status=201, data=data, safe=False)


class SectionView(LoginRequiredMixin, StaffUserRequiredMixin,View):
    login_url = "adminapp:login"

    @method_decorator(csrf_exempt)
    def get(self, request, pk):
        get_object_or_404(Section, id=pk).delete()
        return HttpResponse(status=201)


class SectionDetailView(LoginRequiredMixin, StaffUserRequiredMixin,View):
    login_url = "adminapp:login"

    def get(self, request, pk):
        section = get_object_or_404(Section, id=pk)
        return JsonResponse(data=serializers.serialize("json", [section]), safe=False)

    def post(self, request, pk):
        section = get_object_or_404(Section, id=pk, module__created_by=request.user)
        section.title = request.POST.get("section-name", section.title)
        section.content_type = request.POST.get("section-type", section.content_type)
        section.content = request.POST.get("section-content", section.content)
        section.save()
        return JsonResponse(data=serializers.serialize("json", [section]), safe=False)


class EditSectionView(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    template = "adminapp/edit-section.html"

    def get(self, request, pk):
        obj = get_object_or_404(Section.objects.select_related('module'), id=pk)
        types = Section.TYPE_CHOICES
        context ={'section':obj,'types':types}
        return render(request, self.template, context)
    
    def post(self, request, pk):
        print(request.POST)
        obj = get_object_or_404(Section.objects.select_related('module'), id=pk)
        data = request.POST
        obj.title = data.get('section-name', obj.title)
        obj.content_type = data.get('section-type', obj.content_type)
        obj.content = data.get('content', obj.content)
        obj.video_link = data.get('video_link', obj.video_link)
        obj.save()
        success_url = f"/adminapp/add-modules/{obj.module.course.id}"
        return JsonResponse(data={'success_url':success_url},status=201,safe=False)

class ModuleDetail(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"

    def post(self, request, pk):
        module = get_object_or_404(Module, id=pk)
        module.title = request.POST.get("name", module.title)
        module.save()
        return JsonResponse(status=201, data={}, safe=False)

    


class DeleteModule(LoginRequiredMixin, StaffUserRequiredMixin,View):
    login_url = "adminapp:login"

    def get(self, request, pk):
        obj = get_object_or_404(Module, id=pk)
        obj.delete()
        return JsonResponse(status=200, data={}, safe=False)


class Commissions(LoginRequiredMixin, PermissionRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    template_name = "commisions.html"
    permission_required = get_commision_permissions()

    def get(self, request):
        signup_bonus = SignUpBonus.objects.first()
        referal_bonus = SignUpRefferalCommission.objects.first()
        direct_sale_bonus = DirectShareSaleCommission.objects.first()
        parent_sale_bonus = ParentSaleCommision.objects.first()
        context = {
            "signup_bonus": signup_bonus.bonus,
            "referal_bonus": referal_bonus.amount,
            "direct_sale_bonus": direct_sale_bonus.percentage,
            "parent_sale_bonus": parent_sale_bonus.percentage,
        }
        return render(request, self.template_name, context)


class OrderListView(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    template_name = "adminapp/orders.html"

    def get(self, request):
        orders = PaymentLink.objects.all().order_by('-id')
        context = {'orders': orders}
        return render(request, self.template_name,context)

class VideoListView(LoginRequiredMixin, StaffUserRequiredMixin,View):
    login_url = "adminapp:login"
    template_name = "adminapp/videos.html"

    def get(self, request):
        video_files = S3VideoUpload.objects.filter(is_active=True)
        context = {'video_files':video_files}
        return render(request, self.template_name, context)


class VideoCreateView(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    template_name = "adminapp/add-videos.html"
    
    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        video_file = request.FILES['video_file']
        video_name = request.POST['name']
        if settings.USE_S3:
            upload = S3VideoUpload(file=video_file,name=video_name)
            upload.save()
            video_url = upload.file.url
        else:
            upload = FileSystemStorage()
            filename = upload.save(video_file.name, video_file)
            video_url = upload.url(filename)
        return redirect('adminapp:videodetail', upload.id)

class VideoDetailView(LoginRequiredMixin,StaffUserRequiredMixin, View):
    login_url = "adminapp:login"
    template_name = "adminapp/video-detail.html"

    def get(self, request, pk):
        video = get_object_or_404(S3VideoUpload, id=pk)
        context = {'video':video}
        return render(request, self.template_name, context)