def get_commision_permissions():
    return (
        "add_directsharesalecommission",
        "change_directsharesalecommission",
        "delete_directsharesalecommission",
        "view_directsharesalecommission",
        "add_parentsalecommision",
        "change_parentsalecommision",
        "delete_parentsalecommision",
        "view_parentsalecommision",
        "add_signupbonus",
        "change_signupbonus",
        "delete_signupbonus",
        "view_signupbonus",
        "add_signuprefferalcommission",
        "change_signuprefferalcommission",
        "delete_signuprefferalcommission",
        "view_signuprefferalcommission",
    )
