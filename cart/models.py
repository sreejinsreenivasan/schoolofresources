from django.db import models

# Create your models here.


class Cart(models.Model):
    user = models.OneToOneField(
        "app.User", on_delete=models.CASCADE, related_name="user_cart"
    )
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def clear(self):
        self.items.all.delete()
        self.save()


class CartItem(models.Model):
    cart = models.ForeignKey(
        "cart.Cart", on_delete=models.CASCADE, related_name="items"
    )
    product = models.ForeignKey("courses.Course", on_delete=models.DO_NOTHING)
    quatity = models.IntegerField(default=1)

    def increment(self, quatity):
        self.quatity += self.quatity + quatity
        self.save()

    def decrement(self, quatity):
        current_quantity = self.quatity
        if current_quantity != 0:
            self.quatity -= self.quatity + quatity
            self.save()
        else:
            raise ValueError

    @property
    def remove(self):
        self.delete()


class WishList(models.Model):
    user = models.OneToOneField(
        "app.User", related_name="wishlist", on_delete=models.CASCADE
    )

    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def clear(self):
        self.wishlist_items.all().delete()


class WishListItem(models.Model):
    wishlist = models.ForeignKey(
        WishList, on_delete=models.CASCADE, related_name="wishlist_items"
    )

    product = models.ForeignKey(
        "courses.Course", on_delete=models.CASCADE, related_name="wishlisted_users"
    )
