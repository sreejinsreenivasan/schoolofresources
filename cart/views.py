from django.shortcuts import render
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class CartView(LoginRequiredMixin, View):
    login_url = "app:login"
    template = "cart.html"

    def get(self, request):
        return render(request, self.template)