from django.contrib import admin
from .models import Cart, CartItem, WishList, WishListItem

# Register your models here.

admin.site.register(Cart)
admin.site.register(CartItem)
admin.site.register(WishList)
admin.site.register(WishListItem)