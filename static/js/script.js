function sideBar() {
  var sidebar = document.getElementById("sidebar-menu");
  var sidebar_container = document.getElementById("side-bar-container");
  var topbar = document.getElementById("top-bar");
  var close = document.getElementById("close-btn");
  sidebar_container.classList.toggle("side-bar-container-offset");
  close.classList.toggle("view-close-btn");
  sidebar.classList.toggle("sidebar-offset");
  topbar.classList.toggle("top-bar-offset");
}
function loderView() {
  var loader = document.getElementById("loader25");
  loader.classList.toggle("d-flex");
}

function updateCartValue(increment) {
  const cart = document.getElementById("cart-count");
  const count = parseInt(cart.innerText);
  cart.innerHTML = count + increment;
}

// function getgeoloation() {
//   var xhr = new XMLHttpRequest();
//   xhr.open("GET", 'https://freegeoip.app/json/",true');
//   xhr.send();
//   xhr.onreadystatechange = function () {
//     if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
//       const res = JSON.parse(this.response);
//       return res;
//     }
//   };
// }

// function getcountries() {
//   const url =
//     "https://restcountries.eu/rest/v2/all?fields=name;callingCodes;alpha2Code";
//   const res = fetch(url);
//   return res;
// }

// function getphoneCode() {
//   const geodata = getgeoloation();
//   const countries = getcountries();
//   countries
//     .then((res) => res.json())
//     .then((data) => {
//       const country = data.find((e) => e.alpha2code === geodata.country_code);
//       return country;
//     });
// }
