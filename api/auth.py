from rest_framework.generics import CreateAPIView, GenericAPIView
from rest_framework.views import APIView
from .userserializer import UserSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from .validators import SignupValidation
from project.exceptions import *
from app.models import User, SignupVerification
from django.db import IntegrityError
from oauth2_provider.settings import oauth2_settings

# from braces.views import CsrfExemptMixin
from oauth2_provider.views.mixins import OAuthLibMixin
from django.db import transaction
import json
from project.celery import app as celery_app


class SignUpView(SignupValidation, CreateAPIView):
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        try:
            self.validate_data(data)
            if serializer.is_valid(raise_exception=True):
                try:
                    user = serializer.save()
                    celery_app.send_task(
                        "send_signup_mail", kwargs={"recipient_list": [user.email]}
                    )
                    return Response(
                        data=UserSerializer(
                            user, context=self.get_serializer_context()
                        ).data,
                        status=status.HTTP_201_CREATED,
                    )
                except IntegrityError as e:
                    response = Response(
                        data={
                            "email": ["user with this email address already exists."]
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )
        except EmailValidationErrror as e:
            response = Response(
                data={"email": [str(e)]}, status=status.HTTP_400_BAD_REQUEST
            )
        except PhoneNumberVaildationError as e:
            response = Response(
                data={"phone": [str(e)]}, status=status.HTTP_400_BAD_REQUEST
            )
        except FirstNameValidationError as e:
            response = Response(
                data={"first_name": [str(e)]}, status=status.HTTP_400_BAD_REQUEST
            )
        except PasswordValidationError as e:
            response = Response(
                data={"password": [str(e)]}, status=status.HTTP_400_BAD_REQUEST
            )
        return response


class ConfirmProfile(APIView):
    def post(self, request):
        try:
            code = SignupVerification.objects.get(uid=request.data["unique-code"])
            user = code.user
            with transaction.atomic():
                user.is_active = True
                code.delete()
                user.save()
                response = Response(status=status.HTTP_200_OK)
        except SignupVerification.DoesNotExist:
            response = Response(
                data="Account Email Address Confirmation Failed",
                status=status.HTTP_400_BAD_REQUEST,
            )
        return response
