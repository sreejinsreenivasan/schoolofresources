from django.urls import path, include
from .auth import SignUpView
from .views import (
    CartView,
    WishListView,
    AddWishListItem,
    ClearWishList,
    CourseView,
    CourseDetailView,
    EnrolledCoursesView,
    EnrolledCourseDetailView,
    CategoryView,
)


app_name = "api"

urlpatterns = [
    path("auth/signup/", SignUpView.as_view()),
    path("cart/<int:pk>", CartView.as_view()),
    path("wishlist/<int:pk>", WishListView.as_view()),
    path("add-to-wishlist", AddWishListItem.as_view()),
    path("clear-wishlis/", ClearWishList.as_view()),
    path("courses/", CourseView.as_view()),
    path("course/<str:id>", CourseDetailView.as_view()),
    path("mycourses/", EnrolledCoursesView.as_view()),
    path("enrolled-course/<int:pk>", EnrolledCourseDetailView.as_view()),
    path("categories", CategoryView.as_view())
    # path("add-to-cart/"),
    # path("remove-from-cart/"),
    # path("clear-cart"),
    # path("add-quantity"),
    # path("remove-quantity"),
]
