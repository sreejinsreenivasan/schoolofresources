from rest_framework import generics
from .serializers import (
    CartSerializer,
    CartItemSerializer,
    WishListSerializer,
    WishListItemSerializer,
    CourseSerializer,
    EnrolledCourseDetailSerializer,
    CategorySerializer,
    EnrolledCourseSerializer,
)
from cart.models import Cart, CartItem, WishList, WishListItem
from rest_framework.permissions import IsAuthenticated
from .permissions import IsOwner
from rest_framework.response import Response
from rest_framework import status
from courses.models import Course, Enrollment, Category
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from django.core.exceptions import ValidationError


class CategoryView(generics.ListAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class CartView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated, IsOwner]
    serializer_class = CartSerializer
    queryset = Cart.objects.all()


class CartUpdateView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated, IsOwner]

    def post(self, request, pk):
        cart, created = Cart.objects.get_or_create(user=request.user)
        product = Course.objects.get(id=pk)
        if created:
            cart_item = CartItem.objects.create(product=product, quantity=1, cart=cart)
        else:
            cart_item = CartItem.objects.get(cart=cart, product=product)
        return Response(
            data=CartItemSerializer(
                cart_item, context=self.get_serializer_context()
            ).data
        )


class ClearCartView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated, IsOwner]

    def delete(self, request, pk):
        cart = get_object_or_404(Cart, pk)
        cart.clear

        return Response(status=status.HTTP_200_OK)


class WishListView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated, IsOwner]
    serializer_class = WishListSerializer
    queryset = WishList.objects.all()


class AddWishListItem(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        data = request.data
        product = get_object_or_404(Course, data["product_id"])
        wishlist = request.user.wishlist
        item, created = WishListItem.objects.get_or_create(
            wishlist=wishlist, product=product
        )
        return Response(
            data=WishListItemSerializer(
                item, context=self.get_serializer_context()
            ).data,
            status=status.HTTP_201_CREATED,
        )


class ClearWishList(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        wishlist = request.user.wishlist
        wishlist.clear
        return Response(data="ok", status=status.HTTP_200_OK)


class CourseView(generics.ListAPIView):
    serializer_class = CourseSerializer
    queryset = Course.objects.filter(status="ACTIVE")


class EnrolledCoursesView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        courses = Enrollment.objects.filter(user=request.user)
        return Response(
            data=EnrolledCourseSerializer(
                courses, context=self.get_serializer_context()
            ).data,
            status=status.HTTP_200_OK,
        )


class CourseDetailView(generics.GenericAPIView):
    # permission_classes = [IsAuthenticated, IsOwner]
    serializer_class = CourseSerializer

    def get(self, request, id):
        try:
            course = get_object_or_404(Course, uid=id)
            return Response(
                data=CourseSerializer(
                    course, context=self.get_serializer_context()
                ).data,
                status=status.HTTP_200_OK,
            )
        except ValidationError:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class EnrolledCourseDetailView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated, IsOwner]

    def get(self, request, pk):
        obj = get_object_or_404(Enrollment, id=pk)
        return Response(
            data=EnrolledCourseDetailSerializer(
                obj, context=self.get_serializer_context()
            ).data,
            status=status.HTTP_200_OK,
        )
