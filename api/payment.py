from rest_framework import generics, serializers, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from courses.models import Course
import razorpay
from decouple import config


class BuySerializer(serializers.Serializer):
    currency = serializers.CharField()
    course_id = serializers.IntegerField()


class BuyCourseView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):

            data = serializer.validated_data
            course = get_object_or_404(Course, id=data["course_id"])
            amount = course.offer_price * 100
            currency = data["currency"]
            RZP_KEY = config("RAZORPAY_TEST_KEY")
            client = razorpay.Client(auth=(RZP_KEY, config("RAZORPAY_TEST_SECRET_KEY")))
            payment = client.order.create(
                {"amount": round(amount), "currency": currency, "payment_capture": 1}
            )
            data = {"course_id": course.id, "payment": payment, "rzp_key": RZP_KEY}
            return Response(data=data, status=status.HTTP_201_CREATED)
