from rest_framework import serializers
from app.models import User, Trainer
from .validators import SignupValidation
from project.exceptions import *
from app.utils import create_random_string
from cart.models import Cart, CartItem, WishList, WishListItem
from courses.models import Course, Enrollment, Module, Section, Category


class TrainerSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField("get_full_name")

    class Meta:
        model = Trainer
        fields = ["id", "full_name"]

    def get_full_name(self, instance):
        return instance.user.full_name


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "name"]


class CartItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItem
        fields = ["product", "quantity"]


class CartSerializer(serializers.ModelSerializer):

    cart_items = CartItemSerializer()

    class Meta:
        model = Cart
        fields = ["user", "created_at", "cart_items"]
        extra_kwargs = {"user": {"default": serializers.CurrentUserDefault()}}

    def get_cart_items(self, instance):
        return instance.items.all()


class CourseSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    trainer = TrainerSerializer()

    class Meta:
        model = Course
        fields = [
            "uid",
            "name",
            "price",
            "offer_price",
            "trainer",
            "category",
            "description",
            "course_image",
            "status",
        ]


class WishListItemSerializer(serializers.ModelSerializer):
    product = CourseSerializer()

    class Meta:
        model = WishListItem
        fields = ["product"]


class WishListSerializer(serializers.ModelSerializer):
    wishlist_items = WishListItemSerializer(read_only=True, many=True)

    class Meta:
        model = WishList
        fields = ["created_at", "wishlist_items"]


class AddTutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trainer
        fields = "__all__"


class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = ["title", "content_type"]


class ModuleSerializer(serializers.ModelSerializer):
    sections = serializers.SerializerMethodField("get_sections")

    class Meta:
        model = Module
        fields = ["title", "sections"]

    def get_sections(self, instance):
        return instance.sections.all()


class CourseDetailSerializer(serializers.ModelSerializer):
    modules = serializers.SerializerMethodField("get_modules")

    class Meta:
        model = Course
        fields = [
            "uid",
            "name",
            "price",
            "offer_price",
            "trainer",
            "category",
            "description",
            "course_image",
            "status",
            "modules",
        ]

    def get_modules(self, instance):
        module_data = []
        for m in instance.modules.all():
            sections = []
            for s in m.sections.all():
                sections.append({"title": s.titlde, "content_type": s.content_type})
            module_data.append({"name": m.title, "sections": sections})

        return module_data


class EnrolledCourseDetailSerializer(serializers.ModelSerializer):
    course = CourseDetailSerializer()
    modules = serializers.SerializerMethodField("get_modules")

    class Meta:
        model = Enrollment
        fields = [
            "course",
            "created_at",
            "is_completed",
            "last_viewed_section",
            "last_viewed",
            "uid",
            "modules",
        ]

    def get_modules(self, instance):
        return instance.get_modules_with_sections


# class EnrolledSectionSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Section
#         fields = ["title", "content_type", "content"]


# class EnrolledModuleSerializer(serializers.ModelSerializer):
#     sections = EnrolledSectionSerializer(many=True)

#     class Meta:
#         model = Module
#         fields = ["title", "sections"]


class EnrolledCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enrollment
        # fields = "__all__"
        exclude = ["user", "course"]
