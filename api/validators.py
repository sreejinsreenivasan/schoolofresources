from project.exceptions import *
import re
from rest_framework.response import Response
from rest_framework import status


class SignupValidation:
    email = None
    phone = None
    first_name = None
    last_name = None
    password = None

    def set_values(self, data):
        self.email = data["email"]
        self.first_name = data["first_name"]
        self.last_name = data["last_name"]
        self.phone = data["phone"]
        self.password = data["password"]

    def validate_data(self, data):
        self.set_values(data)
        self._validate_email()
        self.validate_phone_number()
        self.validate_f_name()
        self.validate_l_name()
        self.validate_password()

    def _validate_email(self):
        regrex = r"^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"
        match_re = re.compile(regrex)
        if not re.search(match_re, self.email):
            raise EmailValidationErrror("Email is not valid!")
        pass

    def validate_phone_number(self):
        phone_reg = r"^[6-9]\d{9}$"
        match_re = re.compile(phone_reg)
        res = re.search(match_re, self.phone)
        if not res:
            raise PhoneNumberVaildationError("Phone number is not valid!")
        pass

    def validate_f_name(self):
        if len(self.first_name) < 3:
            raise FirstNameValidationError("First name is too short!")

    def validate_l_name(self):
        if len(self.last_name) < 1:
            raise LastNameValidationError("Last name is too short!")

    def validate_password(self):
        reg = (
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?& ])[A-Za-z\d@$!#%*?&]{8,18}$"
        )
        match_re = re.compile(reg)
        res = re.search(match_re, self.password)
        if not res:
            raise PasswordValidationError(
                "Password should contain atleast one uppercase, one lowercase, a number and a special character"
            )
