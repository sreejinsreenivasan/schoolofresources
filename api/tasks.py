from celery import shared_task
from app.email import send_email, send_registration_confirm_mail
from app.utils import generate_random_number
from cart.models import Cart, WishList
from app.models import User, SignupVerification
from typing import List
from django.db import IntegrityError
from adminapp.models import (
    SignUpBonus,
    SignUpRefferalCommission,
    DirectShareSaleCommission,
    ParentSaleCommision,
)


@shared_task(name="send_signup_mail")
def send_signup_email(reciepient_list: List, user_id):
    user = User.objects.get(id=user_id)
    code, created = SignupVerification.objects.get_or_create(user=user)
    send_registration_confirm_mail(reciepient_list, code.uid)


@shared_task(name="create_user_cart")
def create_cart(user_id):
    user = User.objects.get(id=user_id)
    try:
        cart, created = Cart.objects.get_or_create(user=user)
    except IntegrityError:
        pass


@shared_task(name="create_user_wishlist")
def create_wishlist(user_id):
    user = User.objects.get(id=user_id)
    try:
        wishlist, created = WishList.objects.get_or_create(user=user)
    except IntegrityError:
        pass


@shared_task(name="set_signup_commission")
def set_parent_signup_commision(user_id, referral_id):
    user = User.objects.get(id=user_id)
    try:
        referred_person = User.objects.get(refferal_code=referral_id)
        signup_commission = SignUpRefferalCommission.objects.first()
        amount = signup_commission.amount
        referred_person.primary_wallet += amount
        user.refferal_person = referred_person
        referred_person.save()
        user.save()
    except User.DoesNotExist:
        pass
