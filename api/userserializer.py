from rest_framework import serializers
from app.models import User
from .serializers import CartSerializer, WishListSerializer
from app.utils import create_random_string
from project.celery import app as celery_app
from adminapp.models import SignUpBonus


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "email",
            "phone",
            "country",
            "country_code",
            "first_name",
            "last_name",
            "refferal_code",
            "password",
            "primary_wallet",
            "discount_wallet",
        ]
        read_only_fields = [
            "primary_wallet",
            "discount_wallet",
        ]
        extra_kwargs = {"password": {"write_only": True}}

    def validate(self, data):
        email = data["email"]
        first_name = data["first_name"]
        phone = data["phone"]
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                {"email": "User with this Email ID already exists."}
            )
        elif User.objects.filter(phone=phone).exists():
            raise serializers.ValidationError(
                {"phone": "User with this Phonenumber already exists."}
            )
        elif User.objects.filter(first_name=first_name).exists():
            raise serializers.ValidationError(
                {"first_name": "User with this First Name already exists."}
            )
        return data

    def create(self, validated_data):

        user = User.objects.create_user(
            email=validated_data["email"],
            username=validated_data["email"],
            password=validated_data["password"],
            phone=validated_data["phone"],
            country=validated_data["country"],
            country_code=validated_data["country_code"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"],
            refferal_code=create_random_string(48),
        )
        signup_bonus = SignUpBonus.objects.first()
        user.primary_wallet = signup_bonus.bonus
        user.save()
        celery_app.send_task("create_user_cart", kwargs={"user_id": user.id})
        celery_app.send_task("create_user_wishlist", kwargs={"user_id": user.id})

        return user