from django.views import View
from django.shortcuts import render, redirect, HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin
from typing import Dict
from .utils import (
    validate_login,
    SignUpValidation,
    get_counrty_and_code,
    get_geolocation,
    generate_random_number,
)
from django.contrib import messages
import json
from .models import User, ResetPasswordOTP
from django.core import serializers
import time
from .email import send_email
from .aws import send_otp
from django.db import transaction
from project.celery import app as celery_app


class SignUpView(View, SignUpValidation):

    template_name = "signup.html"

    def get(self, request):
        print(request.session.get("redirect_url"))
        if request.user.is_authenticated:
            return redirect("app:home")

        return render(request, self.template_name)

    def post(self, request):
        self.set_values(request.POST)
        refferal_code = request.POST.get("refferal_code", "")
        user = self.save_user()
        if isinstance(user, User):
            if len(refferal_code) != 0:
                try:
                    with transaction.atomic():
                        refferal_person = User.objects.get(refferal_code=refferal_code)
                        refferal_person.discount_wallet += 100
                        user.refferal_person = refferal_person
                        refferal_person.save()
                        user.save()
                        celery_app.send_task(
                        "send_signup_mail", kwargs={"recipient_list": [user.email]}
                        )
                except Exception as e:
                    print(e)
                    transaction.rollback()
            user = authenticate(username=user.email, password=self.password)
            if user and user is not None:
                login(request, user)
            redirect_url = request.session.get("redirect_url", None)
            if redirect_url is not None:
                del request.session["redirect_url"]
            return JsonResponse(
                status=201,
                data={"redirect_url": redirect_url},
                safe=False,
            )
        return JsonResponse(status=200, data=user, safe=False)


class RefferalSignUp(View, SignUpValidation):
    template = "signup.html"

    def get(self, request, code):
        if request.user.is_authenticated:
            return redirect("app:home")
        return render(request, self.template, {"refferal_code": code})


class LoginView(View):
    template_name = "login.html"
    next_url = None

    def get_next_url(self, request):
        next_url = request.GET.get("next", None)
        if next_url is not None:
            self.next_url = str(next_url)
            request.session["redirect_url"] = self.next_url

    def get(self, request):
        self.get_next_url(request)
        if request.user.is_authenticated:
            return redirect("app:home")
        return render(request, self.template_name)

    def post(self, request):
        validated_data = validate_login(
            {"email": request.POST["email"], "password": request.POST["password"]}
        )
        if not type(validated_data) == dict:
            messages.add_message(request, messages.WARNING, validated_data)
            return redirect("app:login")

        user = authenticate(
            username=validated_data.get("email"),
            password=validated_data.get("password"),
        )

        if user and user is not None:
            login(request, user)
            remember_me = request.POST.get("remember-me", False)
            session_expiry = 0
            if remember_me:
                session_expiry = 1209600
            request.session.set_expiry(session_expiry)
            redirect_url = request.session.get("redirect_url", None)

            if redirect_url is not None:
                del request.session["redirect_url"]
                return redirect(redirect_url)
            else:
                return redirect("app:home")
        else:
            messages.add_message(request, messages.ERROR, "Wrong credentials")
            return redirect("app:login")


def Logout(request):
    logout(request)
    return HttpResponse(status=200)


class UpdateProfileView(LoginRequiredMixin, View):
    login_url = "app:login"
    template_name = "update-profile.html"

    def get(self, request):
        user = request.user
        return render(request, self.template_name, {"user": user})


class RefferView(LoginRequiredMixin, View):
    login_url = "app:login"
    template = "reffer-user.html"

    def get(self, request):
        context = {
            "url": f"https://schoolofresources.com/sign-up/{request.user.refferal_code}"
        }
        return render(request, self.template, context)


class ResetPassword(View, SignUpValidation):
    template = "resetpass.html"

    def get(self, request):
        return render(request, self.template)

    def post(self, request):
        submitted_otp = request.POST["otp"]
        password = request.POST["password"]
        second_password = request.POST["repeat-password"]
        self.password = password
        validated_data = self.validate_password()
        if self.has_errors:
            return JsonResponse(
                status=400, data={"error": validated_data["password_error"]}, safe=False
            )
        try:
            otp_instance = ResetPasswordOTP.objects.get(generated_otp=submitted_otp)
            user = otp_instance.user
            if self.password == second_password:
                user.set_password(self.password)
                user.save()
                otp_instance.delete()
                return JsonResponse(status=201, data={}, safe=False)
        except ResetPasswordOTP.DoesNotExist:
            return JsonResponse(status=400, data={"error": "Invalid OTP"}, safe=False)

        return JsonResponse(status=200, data={}, safe=False)


class SendOTP(View, SignUpValidation):
    user = None
    location = ""

    def post(self, request):
        reachable = request.POST.get("reachable")
        self.location = request.POST.get("geodata")
        if len(reachable) == 0:
            return JsonResponse(
                status=400,
                data={"error": "field should not be empy"},
                safe=False,
            )

        if reachable.isdigit():
            self.phone = reachable
            try:
                if request.user.is_authenticated:
                    self.user = request.user
                else:
                    self.user = User.objects.get(phone=self.phone)

                validated_data = self.validate_phone()
                if not self.has_errors:
                    coutries = get_counrty_and_code()
                    calling_code = list(
                        filter(lambda x: x["alpha2Code"] == self.location, coutries)
                    )[0]["callingCodes"][0]
                    otp = generate_random_number()
                    ResetPasswordOTP.objects.create(user=self.user, generated_otp=otp)
                    send_otp(
                        str(calling_code + self.phone),
                        f"{otp} is your One Time Password for resetting your password. Dont share this with anyone",
                    )
                    return JsonResponse(
                        status=201,
                        data={"error": "", "reachable": self.phone},
                        safe=False,
                    )
                else:
                    return JsonResponse(
                        status=400,
                        data={"error": validated_data["phone_error"]},
                        safe=False,
                    )
            except User.DoesNotExist:
                return JsonResponse(
                    status=400,
                    data={"error": "Acount with this phone number not exist!"},
                    safe=False,
                )
        else:
            self.email = reachable
            try:
                if request.user.is_authenticated:
                    self.user = request.user
                else:
                    self.user = User.objects.get(email=self.email)
                validated_data = self.validate_email()
                if not self.has_errors:
                    otp = generate_random_number()
                    ResetPasswordOTP.objects.create(user=self.user, generated_otp=otp)
                    send_email(
                        "Password Reset",
                        f"{otp} is your OTP generated for resetting password",
                        [validated_data["email"]],
                    )
                    return JsonResponse(
                        status=201,
                        data={"error": "", "reachable": self.email},
                        safe=False,
                    )

                else:
                    return JsonResponse(
                        status=400,
                        data={"error": validated_data["email_error"]},
                        safe=False,
                    )
            except User.DoesNotExist:
                return JsonResponse(
                    status=400,
                    data={"error": "acount with this email does not exist!"},
                    safe=False,
                )