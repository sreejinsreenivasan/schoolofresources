from django.urls import path
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .auth import (
    SignUpView,
    LoginView,
    Logout,
    RefferView,
    RefferalSignUp,
    ResetPassword,
    SendOTP,
)
from .views import (Index, Home,MakePayment,PaymentSuccessView,SuccessPageView, AboutUsView,
 DiscoverView,ReferEarnView,ContactView,TermsAndConditionsView,PrivacyPolicyView)
from .common import ComingSoon

app_name = "app"


urlpatterns = [
    path("sign-up/", SignUpView.as_view(), name="signup"),
    path("sign-up/<str:code>", RefferalSignUp.as_view(), name="refferal_signup"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", Logout, name="logout"),
    path("reset-password/", ResetPassword.as_view(), name="reset-password"),
    path("", Index.as_view(), name="index"),
    path("home/", Home.as_view(), name="home"),
    path("invite/", RefferView.as_view(), name="invite"),
    path("sent-otp/", SendOTP.as_view(), name="sent-otp"),
    # footer links
    path("about-us", AboutUsView.as_view(), name="aboutus"),
    path("refer-and-earn", ReferEarnView.as_view(), name="refer&earn"),
    path("work-from-home", ComingSoon.as_view(), name="workfromhome"),
    path("contact-us", ContactView.as_view(), name="contactus"),
    path("franchisee", ComingSoon.as_view(), name="franchisee"),
    path("terms-and-conditions", TermsAndConditionsView.as_view(), name="termsandconditions"),
    path("privacy-policy", PrivacyPolicyView.as_view(), name="privacypolicy"),
    path("coming-soon", ComingSoon.as_view(), name="comingsoon"),
    path("discover", DiscoverView.as_view(), name="discover"),

    
    path('checkout/<str:id>', MakePayment.as_view(), name="checkout"),
    path("payment-success-no-auth", PaymentSuccessView.as_view(), name="payment-success"),
    path("payment-success/<str:id>",SuccessPageView.as_view(),name="success"),



]
