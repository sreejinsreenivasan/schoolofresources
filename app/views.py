from django.shortcuts import render, redirect, HttpResponse,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from courses.models import Course, Enrollment, Purchase
from adminapp.models import PaymentLink
from django.core.exceptions import ValidationError
from django.http import JsonResponse, Http404
import razorpay
from decouple import config
from django.db import transaction
from django.contrib.auth import get_user_model
from project.celery import app as celery_app
from django.db.models import Q


User = get_user_model()


class Index(View):

    template = "new-index.html"

    def get(self, request):
        courses = Course.objects.filter(status="ACTIVE")[:6]
        context = {"courses": courses}
        return render(request, self.template, context)


class Home(LoginRequiredMixin, View):
    login_url = "app:login"
    tempalte_name = "home.html"

    def get(self, request):
        courses = request.user.enrolled_courses.all()
        context = {"courses": courses}
        return render(request, self.tempalte_name, context)

class MakePayment(View):
    template = "checkout-no-login.html"

    def get(self, request, id):
        try:
            payment = get_object_or_404(PaymentLink, uuid=id)
            try:
                en = Enrollment.objects.get(user__email=payment.email,course=payment.course)
                if en and en is not None:
                    print(en)
                    return redirect('courses:continue', en.uid)
                if payment.status == 'CONVERTED':
                    return redirect('courses:continue', en.uid)
            except Enrollment.DoesNotExist:
                course = payment.course
                discount = course.price - payment.offer_price
                total = course.price - discount
                context = {'course': course,'payment':payment,'discount':discount,'total':total}
                if payment.status == 'ACTIVE':
                    return render(request, self.template, context)
                return redirect('courses:coursesall')
        except ValidationError:
            raise Http404()
    
    def post(self, request, id):
        DEBUG = config('DEBUG')
        if DEBUG:
            RZP_KEY = config("RAZORPAY_TEST_KEY")
            RZP_SECRET = config("RAZORPAY_TEST_SECRET_KEY")
        else:
            RZP_KEY = config("RAZORPAY_KEY")
            RZP_SECRET = config("RAZORPAY_SECRET_KEY")
        p = get_object_or_404(PaymentLink, uuid=id)
        course = p.course
        currency = request.POST["currency"]
        amount = p.offer_price * 100
        client = razorpay.Client(auth=(RZP_KEY, RZP_SECRET))
        payment = client.order.create(
            {"amount": round(amount), "currency": currency, "payment_capture": 1}
        )
        data = {"course_id": course.id, "payment": payment, "rzp_key": RZP_KEY,}
        return JsonResponse(data=data, safe=False)

        

class PaymentSuccessView(View):
    def post(self, request):
        shared_link = request.POST.get("share_link", None)
        order = get_object_or_404(PaymentLink, id=request.POST['order'])
        course = get_object_or_404(Course, uid=request.POST["course_id"])
        sections = []
        
        with transaction.atomic():
            order.status = 'CONVERTED'
            order.save()
            try:
                user = User.objects.get(email=order.email)
            except User.DoesNotExist:
                user = User.objects.create(username=order.email, email=order.email)
                user.set_password(order.email)
                user.save()
            try:
                last_viewed_section = sections[0]["id"]
            except:
                last_viewed_section = 0
            enrollment = Enrollment.objects.create(
                user=user,
                course=course,
                sections=sections,
                last_viewed_section=last_viewed_section,
            )
            purchase = Purchase.objects.create(
                enrollment=enrollment,
                payment_id=request.POST["payment_id"],
                order_id=request.POST["order_id"],
                signature=request.POST["signature"],
            )
            print('step 6')
            celery_app.send_task(
                            "send_payment_success", kwargs={"recipient_list": user.email,'payment_id':purchase.payment_id}
                        )
        try:
            if shared_link is not None:
                product = ProductLinks.objects.get(url_hash=shared_link)
                shared_person = product.created_by
                if user != shared_person:
                    shared_person.primary_wallet += (course.offer_price * 20) / 100
                    shared_person.save()
        except ProductLinks.DoesNotExist:
            pass
        try:
            refer_person = course.trainer.user.refferal_person
            refer_person.primary_wallet += (course.offer_price * 5) / 100
            refer_person.save()

        except AttributeError:
            pass
        redirect_url = f"/payment-success/{enrollment.uid}"
        return JsonResponse(data={"redirect_url": redirect_url}, safe=False)


class SuccessPageView(View):
    template = 'payment-success.html'

    def get(self, request, id):
        context = {"e_id":id}
        return render(request, self.template,context)


class AboutUsView(View):
    template = "about-us.html"
    
    def get(self, request):
        return render(request, self.template)



class DiscoverView(View):
    template = "discover.html"
    
    def get(self, request):
        return render(request, self.template)


class ReferEarnView(View):
    template = "refer-and-earn.html"
    
    def get(self, request):
        return render(request, self.template)

class ContactView(View):
    
    template = "contact-us.html"
    
    def get(self, request):
        return render(request, self.template)



class TermsAndConditionsView(View):
    template = "terms-and-conditions.html"
    
    def get(self, request):
        return render(request, self.template)


class PrivacyPolicyView(View):
    template = "privacy-policy.html"
    
    def get(self, request):
        return render(request, self.template)


    