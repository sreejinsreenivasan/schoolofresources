from django.views import View
from django.shortcuts import render


class ComingSoon(View):
    template = "coming-soon.html"

    def get(self, request):

        return render(request, self.template)