from typing import Dict, List
import re, string
import requests, random, math
from .models import User
from django.db.utils import IntegrityError


def validate_login(cred: Dict) -> Dict:
    for key, value in cred.items():
        if len(value) == 0:
            return f"{key} should not be empty"
    return cred


class SignUpValidation:
    email = ""
    email_error = ""
    password = ""
    password_error = ""
    phone = ""
    phone_error = ""
    first_name = " "
    first_name_error = ""
    last_name = ""
    last_name_error = ""
    country = ""
    country_code = ""
    has_errors = False

    def init_dict(self, key1: str, key2: str) -> Dict:
        return {key1: self.__getattribute__(key1), key2: self.__getattribute__(key2)}

    def init_errors(self):
        self.email_error = ""
        self.first_name_error = ""
        self.last_name_error = ""
        self.password_error = ""
        self.phone_error = ""
        return self

    def set_values(self, data):
        self.email = data["email"]
        self.first_name = data["first-name"]
        self.last_name = data["last-name"]
        self.phone = data["phone"]
        self.password = data["password"]
        self.country = data["country"]
        self.country_code = data["country-code"]

    def validate_email(self):
        validated_email = self.init_dict("email", "email_error")
        regrex = "^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"
        match_re = re.compile(regrex)
        if not re.search(match_re, self.email):
            self.has_errors = True
            return {**validated_email, "email_error": "Email in not valid."}
        self.has_errors = False
        return validated_email

    def validate_password(self):
        validated_password = self.init_dict("password", "password_error")
        reg = (
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?& ])[A-Za-z\d@$!#%*?&]{8,18}$"
        )
        match_re = re.compile(reg)
        res = re.search(match_re, self.password)
        if not res:
            self.has_errors = True
            return {
                **validated_password,
                "password_error": "Password should contain atleast one uppercase, one small case, a number and a special character.",
            }
        self.has_errors = False
        return validated_password

    def validate_phone(self):
        validated_phone = self.init_dict("phone", "phone_error")
        phone_reg = "^[6-9]\d{9}$"
        match_re = re.compile(phone_reg)
        res = re.search(match_re, self.phone)
        if not res:
            self.has_errors = True
            return {**validated_phone, "phone_error": "Phone number is invalid!"}

        return validated_phone

    def validate_first_name(self):
        validated_name = self.init_dict("first_name", "first_name_error")
        if len(self.first_name) < 3:
            self.has_errors = True
            return {**validated_name, "first_name_error": "First name is too short"}
        return validated_name

    def validate_lname(self):
        validated_name = self.init_dict("last_name", "last_name_error")
        if len(self.last_name) < 1:
            self.has_errors = True
            return {**validated_name, "last_name_error": "Last name is too short"}
        return validated_name

    def validate_form_data(self):
        return {
            **self.validate_email(),
            **self.validate_password(),
            **self.validate_phone(),
            **self.validate_first_name(),
            **self.validate_lname(),
        }

    def save_user(self):
        d = self.validate_form_data()
        if not self.has_errors:
            user_exists = User.objects.filter(email=self.email).exists()
            if user_exists:
                return {**d, "email_error": "Email Address already exists."}
            try:
                user = User.objects.create(
                    email=self.email,
                    username=self.email,
                    first_name=self.first_name,
                    last_name=self.last_name,
                    phone=self.phone,
                    country=self.country,
                    country_code=self.country_code,
                    refferal_code=create_random_string(8).upper(),
                    discount_wallet=100,
                )
                if user and user is not None:
                    user.set_password(self.password)
                    user.save()
                    return user
            except IntegrityError:
                return {**d, "email_error": "User already exists."}
        else:
            return d


def get_geolocation() -> Dict:
    try:
        url = "https://freegeoip.app/json/"
        headers = {"accept": "application/json", "content-type": "application/json"}
        response = requests.request("GET", url, headers=headers)
        return response.json()
    except Exception as e:
        print(e)
        return None


def get_counrty_and_code() -> List:
    try:
        r = requests.get(
            "https://restcountries.eu/rest/v2/all?fields=name;callingCodes;alpha2Code"
        )
        if r.status_code == 200:
            return r.json()
    except:
        return []


def create_random_string(length: int) -> str:
    letters = string.ascii_letters + string.digits
    return "".join(random.choice(letters) for i in range(length))


def generate_random_number():
    digits = [i for i in range(0, 10)]
    random_str = ""
    for i in range(0, 6):
        index = math.floor(random.random() * 10)
        random_str += str(digits[index])
    return random_str
