from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid

# Create your models here.


class User(AbstractUser):
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    email = models.EmailField(
        blank=True, max_length=254, verbose_name="email address", unique=True
    )
    phone = models.CharField(max_length=12, null=True)
    country = models.CharField(max_length=100)
    country_code = models.CharField(max_length=10, null=True)
    dob = models.CharField(max_length=12, null=True)
    is_active = models.BooleanField(default=True)
    profile_image = models.FileField(upload_to="profile/%d%m%y", null=True)
    about_me = models.TextField(null=True)
    news_letter = models.BooleanField(default=False)
    refferal_code = models.CharField(max_length=50, null=True, unique=True)
    refferal_person = models.ForeignKey(
        "app.User", on_delete=models.PROTECT, related_name="refferals", null=True
    )
    primary_wallet = models.FloatField(default=0.00)
    discount_wallet = models.FloatField(default=0.00)

    @property
    def full_name(self):
        return self.first_name + " " + self.last_name


class Trainer(models.Model):
    user = models.ForeignKey("app.User", on_delete=models.CASCADE)


class ResetPasswordOTP(models.Model):
    user = models.ForeignKey(
        "app.User", on_delete=models.CASCADE, related_name="reset_otp"
    )
    generated_otp = models.CharField(max_length=6)
    created_at = models.DateTimeField(auto_now_add=True)


class SignupVerification(models.Model):
    user = models.ForeignKey(
        "app.User", on_delete=models.CASCADE, related_name="verification_code"
    )

    uid = models.UUIDField(default=uuid.uuid4(), unique=True, editable=False)
