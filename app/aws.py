import boto3
from decouple import config


def send_otp(phone_number: int, message: str):
    client = boto3.client(
        "sns",
        aws_access_key_id=config("SMS_AWS_ACCESS_KEY"),
        aws_secret_access_key=config("SMS_AWS_SECRET_ACCESS_KEY"),
        region_name="ap-south-1",
    )
    client.publish(PhoneNumber=phone_number, Message=message)
