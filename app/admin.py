from django.contrib import admin
from .models import User, Trainer

# Register your models here.

admin.site.register(User)
admin.site.register(Trainer)