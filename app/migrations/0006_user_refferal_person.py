# Generated by Django 3.1.2 on 2020-11-14 07:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_user_refferal_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='refferal_person',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='refferals', to='app.user'),
            preserve_default=False,
        ),
    ]
