# Generated by Django 3.1.2 on 2020-11-14 11:32

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_user_refferal_person'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='refferal_person',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='refferals', to=settings.AUTH_USER_MODEL),
        ),
    ]
