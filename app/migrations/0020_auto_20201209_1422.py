# Generated by Django 3.1.2 on 2020-12-09 08:52

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_auto_20201209_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signupverification',
            name='uid',
            field=models.UUIDField(default=uuid.UUID('dcee63af-393f-49f0-9efa-ef975ffe05e7'), editable=False, unique=True),
        ),
    ]
