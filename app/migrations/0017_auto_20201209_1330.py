# Generated by Django 3.1.2 on 2020-12-09 08:00

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20201209_1329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signupverification',
            name='uid',
            field=models.UUIDField(default=uuid.UUID('8c538ca9-bc62-44af-8d0b-94b67c4f21bb'), editable=False, unique=True),
        ),
    ]
