# Generated by Django 3.1.2 on 2021-03-17 05:53

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0029_auto_20210305_1033'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signupverification',
            name='uid',
            field=models.UUIDField(default=uuid.UUID('cd74dc94-9174-4276-b1e5-519d2dadac10'), editable=False, unique=True),
        ),
    ]
