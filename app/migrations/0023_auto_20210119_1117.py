# Generated by Django 3.1.2 on 2021-01-19 05:47

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0022_auto_20201209_1423'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signupverification',
            name='uid',
            field=models.UUIDField(default=uuid.UUID('c2980b6d-b3d4-4ea5-a5b2-e3e1d4e52014'), editable=False, unique=True),
        ),
    ]
