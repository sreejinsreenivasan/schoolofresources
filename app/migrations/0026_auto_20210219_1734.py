# Generated by Django 3.1.2 on 2021-02-19 12:04

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_auto_20210219_1729'),
    ]

    operations = [
        migrations.AlterField(
            model_name='signupverification',
            name='uid',
            field=models.UUIDField(default=uuid.UUID('7ef9739d-92b8-4e8b-b678-d713ca34ae61'), editable=False, unique=True),
        ),
    ]
