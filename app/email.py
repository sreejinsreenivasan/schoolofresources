from django.core.mail import send_mail
from typing import List, Dict
from decouple import config
from django.template import Context
from django.utils.html import strip_tags
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage


def send_email(subject: str, message: str, recipient_list: List):
    send_mail(
        subject=subject,
        message=message,
        from_email=config("NOREPLYMAIL"),
        recipient_list=recipient_list,
        fail_silently=False,
    )


def generate_mail_template(context: Dict) -> str:
    template = render_to_string("registration-mail.html", context)
    return template


def send_registration_confirm_mail(recipient_list: List, code):
    subject = "Welcome to SchoolofResources! Confirm Your Email"
    html_content = get_template("registration-mail.html").render(
        {"link": f"https://schoolofresources.com/verfiy/?code={code}"}
    )

    msg = EmailMessage(
        subject,
        html_content,
        config("NOREPLYMAIL"),
        recipient_list,
    )
    msg.content_subtype = "html"
    msg.send()

def send_payment_email(recipient_list: List, code):
    subject = "Complete Your Payment | School of Resources"
    html_content = get_template("payment-link.html").render(
            {"link": f"https://schoolofresources.com/checkout/{code}"}
    )
    msg = EmailMessage(
        subject,
        html_content,
        config("NOREPLYMAIL"),
        recipient_list,
    )
    msg.content_subtype = "html"
    msg.send()

def send_payment_confirmation_mail(recipient_list: List, payment_id):
    subject = "Payment Successful "
    html_content = get_template("payment-success-mail.html").render(
            {"payment_id": payment_id}
    )
    msg = EmailMessage(
        subject,
        html_content,
        config("NOREPLYMAIL"),
        recipient_list,
    )
    msg.content_subtype = "html"
    msg.send()