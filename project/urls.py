"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .settings import MEDIA_ROOT, MEDIA_URL
from django.conf.urls.static import static
from app.email import send_registration_confirm_mail





urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("app.urls", namespace="app")),
    path("adminapp/", include("adminapp.urls", namespace="adminapp")),
    path("courses/", include("courses.urls", namespace="courses")),
    path("", include("cart.urls", namespace="cart")),
    path("api/v1/", include("api.urls", namespace="api")),
    path("api/v1/auth/", include("drf_social_oauth2.urls", namespace="drf")),
] + static(MEDIA_URL, document_root=MEDIA_ROOT)
